-- Author: Supun Jayathilake (supunj@gmail.com)

-- Update provincial capitals
UPDATE nodes
SET tags = tags || '"place"=>"city"'::hstore || '"is_city"=>""'::hstore
WHERE nodes.tags ? 'is_in' AND tags->'name' IN ('Kandy', 'Trincomalee', 'Anuradhapura', 'Jaffna', 'Kurunegala', 'Ratnapura', 'Galle', 'Badulla', 'Colombo');

-- Update district capitals
UPDATE nodes
SET tags = tags || '"place"=>"town"'::hstore || '"is_town"=>""'::hstore
WHERE nodes.tags ? 'is_in' AND tags->'name' IN ('Ampara', 'Batticaloa', 'Gampaha', 'Hambantota', 'Kalutara', 'Kegalle', 'Killinochchi', 'Mannar', 'Matale', 'Matara', 'Monaragala', 'Mullaitivu', 'Nuwara Eliya', 'Polonnaruwa', 'Puttalam', 'Vavuniya');

-- Update villages
UPDATE nodes
SET tags = tags || '"place"=>"village"'::hstore || '"is_village"=>""'::hstore
WHERE NOT nodes.tags ?| ARRAY['is_city','is_town'] AND tags->'place' IN ('town', 'city', 'village');

-- Fix ways

-- Add oneway=no when not present
UPDATE ways
SET tags = tags || '"oneway"=>"no"'::hstore
WHERE tags ? 'highway' AND NOT tags ? 'oneway';

-- Update express ways
UPDATE ways
SET tags = tags || '"highway"=>"motorway"'::hstore || '"maxspeed"=>"100"'::hstore || '"oneway"=>"yes"'::hstore
WHERE tags ? 'highway' AND tags->'ref' ~ '^[E]\d+$';

-- Update A class roads
UPDATE ways
SET tags = tags || '"highway"=>"trunk"'::hstore || '"maxspeed"=>"70"'::hstore
WHERE tags ? 'highway' AND tags->'ref' ~ '^[A]\d+$';

-- Update B class roads
UPDATE ways
SET tags = tags || '"highway"=>"primary"'::hstore || '"maxspeed"=>"60"'::hstore
WHERE tags ? 'highway' AND tags->'ref' ~ '^[B]\d+$';

UPDATE ways
SET tags = tags || '"maxspeed"=>"60"'::hstore
WHERE tags ? 'highway' AND tags->'highway' IN ('primary') AND NOT tags->'ref' ~ '^[A-Z]\d+$';

-- Update C class roads
UPDATE ways
SET tags = tags || '"maxspeed"=>"50"'::hstore
WHERE tags ? 'highway' AND (tags->'highway' = 'secondary' OR tags->'highway' = 'secondary_link' OR tags->'highway' = 'trunk_link' OR tags->'highway' = 'primary_link');

UPDATE ways
SET tags = tags || '"maxspeed"=>"40"'::hstore
WHERE tags ? 'highway' AND (tags->'highway' = 'tertiary' OR tags->'highway' = 'tertiary_link');

-- Update other roads
UPDATE ways
SET tags = tags || '"maxspeed"=>"30"'::hstore
WHERE tags ? 'highway' AND tags->'highway' IN ('residential', 'road');
