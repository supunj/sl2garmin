#!/bin/bash

#Author: Supun Jayathilake (supunj@gmail.com)

# Initialize locations
source ./map_locations.sh

echo 'Splitting the map....'
$MKGMAP_JAVACMD $MKGMAP_JAVACMD_OPTIONS $SPLITTER --output=pbf --output-dir=$TEMP_LOC/split_files $TEMP_LOC/sri-lanka-latest.osm.pbf > $TEMP_LOC/splitter.log
